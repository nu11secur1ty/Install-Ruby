
# Default  
```
  yum groupinstall "Development Tools"  
  yum install -y ruby  
  yum install -y gcc make automake autoconf curl-devel openssl-devel zlib-devel httpd-devel apr-devel apr-util-devel sqlite-devel  
  yum install -y ruby-rdoc ruby-devel   
  yum install -y rubygems  
  gem update
```
#
```  
  yum groupinstall -y development  
  curl -L https://raw.githubusercontent.com/nu11secur1ty/ruby-2.2.x/master/ruby_rvm.sh | bash -s stable  
  gpg2 --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3  
  source /etc/profile.d/rvm.sh  
  rvm install 2.2.4  
  gem update
  gem update --system
  gem install rails  
  ruby --version 
  rvm default
```
#
```
 cd /usr/local/rvm/gems/ruby-2.2.4/bin
    echo "export PATH=$(pwd):\$PATH" >> ~/.bashrc

 cd /usr/local/rvm/gems/ruby-2.2.4@global/bin
    echo "export PATH=$(pwd):\$PATH" >> ~/.bashrc

 cd /usr/local/rvm/rubies/ruby-2.2.4/bin
    echo "export PATH=$(pwd):\$PATH" >> ~/.bashrc

 cd /usr/local/rvm/bin
    echo "export PATH=$(pwd):\$PATH" >> ~/.bashrc
```


#!/usr/bin/bash
# Default  
  yum groupinstall "Development Tools"  
  yum install -y ruby  
  yum install -y gcc make automake autoconf curl-devel openssl-devel zlib-devel httpd-devel apr-devel apr-util-devel sqlite-devel  
  yum install -y ruby-rdoc ruby-devel   
  yum install -y rubygems  
  gem update
#
  yum groupinstall -y development  
  gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3   
  curl -sSL https://get.rvm.io | bash -s stable
  source /etc/profile.d/rvm.sh  
  rvm install 2.3.1  
  gem update 
  gem update --system 
  gem install rails  
  ruby --version 
  rvm default 
#
 cd /usr/local/rvm/gems/ruby-2.3.1/bin
    echo "export PATH=$(pwd):\$PATH" >> ~/.bashrc

 cd /usr/local/rvm/gems/ruby-2.3.1@global/bin
    echo "export PATH=$(pwd):\$PATH" >> ~/.bashrc

 cd /usr/local/rvm/rubies/ruby-2.3.1/bin
    echo "export PATH=$(pwd):\$PATH" >> ~/.bashrc

 cd /usr/local/rvm/bin
    echo "export PATH=$(pwd):\$PATH" >> ~/.bashrc

exit 0;

